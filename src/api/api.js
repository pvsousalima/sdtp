import firebase from 'firebase';
import lodash from 'lodash';

let BAD_PARAMETERS = {code:1, message:'bad parameters'};

/**
* API class, responsible for handling parameters and functions to connect and handle data
necessary to firebase.
*/
export default class API {

  constructor(){

    // Initialize Firebase
    let config = {
      apiKey: "AIzaSyBzgu0YSd4u-O5GnhoSsXlt3qmTCCZ-26k",
      authDomain: "eshop-78022.firebaseapp.com",
      databaseURL: "https://eshop-78022.firebaseio.com",
      storageBucket: "eshop-78022.appspot.com",
    };
    // initialize the app
    firebase.initializeApp(config);
  }

  getCategories() {
    function loadJob(res, rej) {
      try {
        firebase.database().ref('categories').on('value', function(categories) {
          res(lodash.toArray(categories.val()).reverse());
        });
      } catch (err){
        rej({error:err.code, message:err.message}); // rejects
      }
    }
    return new Promise(loadJob); //returns the load promise
  }

  getPosts() {
    function loadJob(res, rej) {
      try {
        firebase.database().ref('posts').on('value', function(categories) {
          res(lodash.toArray(categories.val()).reverse());
        });
      } catch (err){
        rej({error:err.code, message:err.message}); // rejects
      }
    }
    return new Promise(loadJob); //returns the load promise
  }

  basicLogin(email, password) {
    function login(res, rej) {
      if(email && password){  // verify parameters
        try {
          firebase.auth().signInWithEmailAndPassword(email, password).then((user)=>{
            res(user);
          });
        } catch (err){
          rej({error:err.code, message:err.message}); // rejects
        }
      } else {
        rej(BAD_PARAMETERS); // rejects bad or invalid parameters
      }
    }
    return new Promise(login); //returns the login promise
  }

  isLogged(){
    function verifyAuth(res,rej){
      try {
        firebase.auth().onAuthStateChanged(function(user) {
          if (user) { // User is signed in.
            res(true);
          } else { // No user is signed in.
            res(false);
          }
        });
      }
      catch(err) {
        rej(err)
      }
    }

    return new Promise(verifyAuth)
  }

}
