// react DOM
import React from 'react';
import ReactDOM from 'react-dom';

// import api
import {API} from '../src/api/';


// app componenets
import App from './App';
import Login from './components/Login'
import Categories from './components/Categories';
import Feed from './components/Feed';
import Profile from './components/Profile';
import Register from './components/Register';

import './index.css';

// for routing
import { Router, Route, browserHistory } from 'react-router'

global.api = new API();

// console.log(API());

// render the main component
ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}/>
    <Route path="login" component={Login}/>
    <Route path="categories" component={Categories}/>
    <Route path="feed" component={Feed}/>
    <Route path="register" component={Register}/>
    <Route path="profile" component={Profile}/>
  </Router>,
  document.getElementById('root')
);
