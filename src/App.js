import React, { Component } from 'react';
import { browserHistory } from 'react-router'
import './App.css';

// mui webkit
import Button from 'muicss/lib/react/button';
import Panel from 'muicss/lib/react/panel';

// materialize webkit
import {Slider, Slide} from 'react-materialize';

class App extends Component {

  handleLoginClick(){
    browserHistory.push('/login')
  }

  handleRegisterClick(){
    browserHistory.push('/register')
  }
  
  render() {
    return (
      <div className="App">

        <Slider>
          <Slide
            src="https://images-eu.ssl-images-amazon.com/images/G/31/img15/Shoes/CatNav/p._V293117552_.jpg">
          </Slide>

          <Slide
            src="http://i.huffpost.com/gen/793571/images/o-CLOTHING-COLOUR-facebook.jpg"
            placement="left">
          </Slide>

          <Slide
            src="http://polario.r.worldssl.net/media/catalog/product/b/i/bicicleta_mazza_bikes_ninne_29_21v_freio_a_disco_mecanico-branco-19.jpg"
            placement="right">
          </Slide>
        </Slider>
        <div style={{padding: '1px'}} />

        <div className="mui--text-display4">Buy. Sell. Trade. </div>
        <div className="mui--text-display1">We present you a new way to buy, sell and trade your goods </div>

        <Panel>
         <Button color="primary" onClick={this.handleLoginClick} style={{float:'center', backgroundColor: '#03A9F4'}}>Login</Button>
         <Button color="primary" onClick={this.handleRegisterClick} style={{float:'center', backgroundColor: '#03A9F4'}}>Register</Button>
         <div style={{padding: '1px'}} />
        </Panel>
      </div>
    );
  }
}

export default App;
