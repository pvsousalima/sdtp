import React, { Component } from 'react';
import { browserHistory } from 'react-router'

// materialize
import {Card, CardTitle, Footer, Preloader, Button, Row, Input} from 'react-materialize';

import Container from 'muicss/lib/react/container';

class Login extends Component {

  constructor (props){
    super(props);
  }

  handleLoginClick(){

    global.api.basicLogin("pvsousalima@gmail.com", "123456").then((user)=>{
      console.log(user)
      if(user){
        browserHistory.push('/categories')
      }
    })
  }

  render() {
    return (
      <div className="App">
      <div className="App-header">
        <div style={{paddingRight:'10px', float:'right'}}>
        </div>
      </div>
        <div>
        <img style={{float:'center', width:'300px', padding:'50px'}} src={"../src/login.png"} alt="logo" />
            <Container style={{marginTop:'30px', width:'60%', borderRadius:'15px', backgroundColor:'white', float:'center'}}>
              <Row>
                  <Input type="email" label="email" s={12} />
                  <Input type="password" label="password" s={12} />
                  <Button className='blue' style={{marginTop:'10px', backgroundColor: '#03a9f4'}} onClick={this.handleLoginClick} > Log In </Button>
              </Row>
            </Container>
        </div>

      </div>
    );
  }

}



export default Login;
