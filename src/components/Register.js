import React, { Component } from 'react';
import { browserHistory } from 'react-router'

// materialize
import {Card, CardTitle, Footer, Preloader, Button, Row, Input, Icon} from 'react-materialize';

import Container from 'muicss/lib/react/container';

class Register extends Component {

  constructor (props){
    super(props);
  }

  handleRegisterClick(){
    global.api.basicLogin("pvsousalima@gmail.com", "123456").then((user)=>{
      if(user){
        browserHistory.push('/categories')
      }
    })
  }

  render() {
    return (
      <div className="App">
      <div className="App-header">
        <div style={{paddingRight:'10px', float:'right'}}>
        </div>
      </div>
        <div>
        <img style={{float:'center', width:'300px', padding:'50px'}} src={"../src/login.png"} alt="logo" />
            <Container style={{width:'60%', borderRadius:'15px', backgroundColor:'white', float:'center'}}>
              <Row>
                  <Input type="email" label="email" s={12} />
                  <Input type="password" label="password" s={12} />
                  <Input type="email" label="CPF" s={12} />
                  <Input s={6} label="Name" validate><Icon>account_circle</Icon></Input>
                  <Input s={6} label="Telephone" validate type='tel'><Icon>phone</Icon></Input>
                  <Button className='blue' style={{marginTop:'10px', backgroundColor: '#03a9f4'}} onClick={this.handleRegisterClick} > Register </Button>
              </Row>
            </Container>
        </div>

      </div>
    );
  }

}



export default Register;
