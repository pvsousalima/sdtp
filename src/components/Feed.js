import React, { Component } from 'react';
import { browserHistory } from 'react-router'

// the style
import './Categories.css';

// mui webkit
import Container from 'muicss/lib/react/container';
import Row from 'muicss/lib/react/row';
import Col from 'muicss/lib/react/col';
import Search from 'react-search'

// materialize
import {Card, CardTitle, Footer, Preloader, Button, NavItem, Icon} from 'react-materialize';

import Grid from './Grid.js';

class Feed extends Component {

  constructor (props){
    super(props);
    this.state = {
      categories: null,
      hasCategories: false,
    };

  }

  handleProfileClick(){
    browserHistory.push('/profile')
  }

  componentDidMount(){
    global.api.getPosts().then((posts)=>{
      console.log(posts)
      this.setState({categories:posts, hasCategories:true})
    })

  }


  // <Card header={<CardTitle reveal image='http://turbo.designwoop.com/uploads/2015/01/17-list-products-film-android-material-design.jpg'/>}
  //     title="Smartphone"
  //     reveal={<p>Here is some more information about this product that is only revealed once clicked on.</p>}>
  //   <p style={{padding:'5px'}}/>
  //   <Panel>
  //     <a>
  //       <img src={"https://ssl.gstatic.com/images/branding/product/1x/avatar_circle_blue_512dp.png"} className="App-logo" alt="logo" />
  //     </a> Pedro Victor
  //   </Panel>
  // </Card>

  renderCategories() {

    if(this.state.hasCategories){
        return (
            <Grid>
                {
                    this.state.categories.map((elm, index) => {
                        return <Card key={index} header={<CardTitle image={elm.image}/>}
                            title={elm.title}
                            reveal={elm.description}>
                            <a href={elm.title}> see more </a>

                        </Card>
                    })
                }
            </Grid>
        )

    //   return this.state.categories.map((elm, index) => {
    //     return <Col >
    //         <Card header={<CardTitle image={elm.image}/>}
    //             title={elm.title}
    //             reveal={elm.description}>
    //             <a href={elm.title}> see more </a>
    //         </Card>
    //     </Col>
    //   })

    } else {
      return <Row>
          <Col l={24}>
              <p style={{marginTop:'40px'}}> <Preloader size='big'/> </p>
          </Col>
      </Row>
    }
  }



  renderFooter(){
    if(this.state.hasCategories){
      return  <Footer style={{backgroundColor:'#03A9F4'}}
        className='example'>
        <h5 className="white-text">eShop</h5>
        <p className="grey-text text-lighten-4">Copyright © 2016 Design Stack</p>
      </Footer>
    }
  }

  render() {
    return (
      <div className="App">

        <div className="App-header">
          <a href="/categories"><img src={"../src/logo.png"} className="App-logo" alt="logo" /></a>
            <div style={{float:'right', paddingRight:'20px'}}>
            <Button floating large className='white' onClick={this.handleProfileClick}>
              <a><img src={"https://ssl.gstatic.com/images/branding/product/1x/avatar_circle_blue_512dp.png"} className="App-logo" alt="logo" /></a>
            </Button>
            </div>
        </div>

        <Container fluid={true}>
            <p style={{padding:'10px'}}/>
            {this.renderCategories()}
        </Container>

        <Button onClick={this.onSetSidebarOpen} floating fab='horizontal' icon='add' className='red' large style={{bottom: '45px', right: '24px'}}/>

        {this.renderFooter()}
      </div>
    );
  }

}





export default Feed;
