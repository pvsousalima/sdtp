import React, { Component } from 'react';
import { browserHistory } from 'react-router'

// materialize
import {Card, CardTitle, Footer, Preloader, Button, Row, Input} from 'react-materialize';

import Container from 'muicss/lib/react/container';

class Profile extends Component {

  constructor (props){
    super(props);
  }

  handleProfileClick(){
    browserHistory.push('/categories')
  }

  handleSignOutClick(){
    browserHistory.push('/')
  }

  render() {
    return (
      <div className="App">
      <div className="App-header">
        <div style={{paddingRight:'10px', float:'right'}}>
        </div>
      </div>
        <div>
        <img style={{float:'center', width:'300px', padding:'50px'}} src={"https://ssl.gstatic.com/images/branding/product/1x/avatar_circle_blue_512dp.png"} alt="logo" />
            <Container style={{marginTop:'30px', width:'60%', borderRadius:'15px', backgroundColor:'white', float:'center'}}>
              <Row>
                <Input  defaultValue='pvsousalima@gmail.com' type="email" label="email" s={12} />
                <Input defaultValue='pvsousalima@gmail.com' type="password" label="password" s={12} />
                <Button className='blue' style={{float:'left', marginTop:'10px', backgroundColor: '#03a9f4'}} onClick={this.handleProfileClick} > Update </Button>
              </Row>
              <Button className='red' style={{float:'right', marginTop:'40px', backgroundColor: '#03a9f4'}} onClick={this.handleSignOutClick} > Sign Out </Button>
            </Container>
        </div>
      </div>
    );
  }

}



export default Profile;
