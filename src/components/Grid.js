import React, { Component } from 'react';
import { browserHistory } from 'react-router'

// the style
import './Categories.css';

// mui webkit
import Container from 'muicss/lib/react/container';
import Row from 'muicss/lib/react/row';
import Col from 'muicss/lib/react/col';
import Search from 'react-search'

// materialize
import {Card, CardTitle, Footer, Preloader, Button, NavItem, Icon} from 'react-materialize';


export default class Grid extends Component {

    mapChildrenToBlock() {

        let children = this.props.children;

        return children.map(elm => {
                return (
                    <div key={Math.random()} style={{
                        width: 400,
                        height: 600,
                        padding: 20,
                    }}>
                        {elm}
                    </div>
                )
        })

        // return React.Children(this.props.children, elm => {
        //     return (
        //         <div key={Math.random()} style={{width: 200, height: 200}}>
        //             {elm}
        //         </div>
        //     )
        // })
    }

    render(){
        // console.log(this.props.children);
        // console.log(this.mapChildrenToBlock());

        let grid = {
            display:'flex',
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'center',
            alignContent: 'center',
            alignItems: 'center',
        }

        return (
            <div style={grid}>
                {this.mapChildrenToBlock()}
                {/* {this.props.children} */}
            </div>
        )
    }
}
